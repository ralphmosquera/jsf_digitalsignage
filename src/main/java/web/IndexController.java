package web;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import model.DwClient;
import model.Entry;
import model.Playlist;
import org.primefaces.context.RequestContext;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;
import java.io.Serializable;
import java.util.List;

@Named
@SessionScoped
@ManagedBean
public class IndexController implements Serializable{

    private List<Entry> entries;
    private List<DwClient> Clients;

    private int selectedClient;


    public int getSelectedClient() {
        return selectedClient;
    }

    public void setSelectedClient(int selectedClient) {
        this.selectedClient = selectedClient;
        getEntriesByClientId();
        RequestContext.getCurrentInstance().update("elist");
    }

    public List<DwClient> getClients() {
        return Clients;
    }

    public void setClients(List<DwClient> clients) {
        Clients = clients;
    }

    private int clientId;
    private String serverIP = "10.0.0.18";
    private String restURI="http://"+serverIP+":8080/thinrestserver/rs/client/get/";
    private String restOnlineClientsURI="http://"+serverIP+":8080/thinrestserver/rs/client";
    private String servletURI="http://"+serverIP+":8080/thinrestserver/entry?";
    private Playlist playlist;
    //servlet
    //http://10.0.0.9:8080/thinrestserver/entry?e_id=1&type=1
    //service
    //http://localhost:8080/thinrestserver/rs/client/get/{id}





    @PersistenceContext
    EntityManager entityManager;
    @Resource
    private UserTransaction userTransaction;



    @PostConstruct
    public void init() {
        getOnlineClient();

    }


    //BUTTON
    public void bt_getClientPlaylist(javax.faces.event.ActionEvent actionEvent) {
        getEntriesByClientId();
        RequestContext.getCurrentInstance().update("elist");
    }

    //clientlist event
    public void onClientChange() {

    }

    //servlet
    public String generateServletURI(long id, int type) {
        String e_id = "e_id=" + id;
        String t = "type=" + type;
        return servletURI + e_id + "&" + t;
    }

    //REST-EntriesFromClientId
    public void getEntriesByClientId(){
        Client client = Client.create();

        WebResource wr = client
                .resource(restURI +selectedClient);
        String jsonResponse = wr.get(String.class);
        Gson gson = new Gson();
        setEntries(gson.fromJson(jsonResponse, new TypeToken<List<Entry>>(){}.getType()));

    }
    //REST-OnlineClients
    public void getOnlineClient(){
        Client client = Client.create();
        WebResource wr = client
                .resource(restOnlineClientsURI);
        String jsonResponse = wr.get(String.class);
        Gson gson = new Gson();
        setClients(gson.fromJson(jsonResponse, new TypeToken<List<DwClient>>(){}.getType()));

    }

    //ClientPlaylistMB
    public String getStringEntriesByClientId(){
        Client client = Client.create();
        ////http://localhost:8080/thinrestserver/rs/client/get/{id}
        WebResource wr = client
                .resource(restURI);
        return wr.get(String.class);
    }


  //http://localhost:8080/thinrestserver/rs/client/get/{id}

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public List<Entry> getEntries() {
        return entries;
    }

    public void setEntries(List<Entry> entries) {
        this.entries = entries;
    }

}
