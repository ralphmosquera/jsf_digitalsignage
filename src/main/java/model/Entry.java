package model;

/**
 * Created by Mosquera on 28.10.2016.
 */
public class Entry{


    private Long id;
    private int type;
    private String TypeAsName;
    private Image image;

    public Entry(int type) {
        this.type = type;

    }

    public Entry() {
    }

    public Long getId() {
        return id;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public String getTypeAsName() {
        if (type == 1) {
            return "IMAGE";
        }
        if (type == 2) {
            return "VIDEO";
        }
        return "UNKNOWN";

    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }


}
