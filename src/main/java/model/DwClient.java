package model;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Puristo on 02.04.2017.
 */
public class DwClient {
    private int id;
    private boolean online;
    private long  lastSeen;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isOnline() {
        return online;
    }

    public void setOnline(boolean online) {
        this.online = online;
    }

    public String getLastSeen() {

        Date convert = new Date(lastSeen);
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE,d MMMM yyyy h:mm,a", Locale.GERMAN);
        String formattedDate = sdf.format(convert);
        return formattedDate;
    }

    public void setLastSeen(long lastSeen) {
        this.lastSeen = lastSeen;
    }
}
