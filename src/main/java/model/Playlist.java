package model;

import java.util.Collection;
import java.util.List;

/**
 * Created by Mosquera on 28.10.2016.
 */
@Deprecated
public class Playlist{

    private Long id;

    private List<Entry> entries;

    public Playlist() {

    }
    public Long getId() {
        return id;
    }

    public Collection<Entry> getEntries() {
        return entries;
    }

    public void setEntries(List<Entry> entries) {
        this.entries = entries;
    }

}
